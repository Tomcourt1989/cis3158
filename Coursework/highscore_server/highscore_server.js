var http = require('http');
const { data } = require('jquery');

var highscores = [];
var next_id = 0;

var server = http.createServer(function(request, response) {
    if(request.url == '/highscores') {
        response.setHeader('Content-Type', 'application/json');
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Access-Control-Allow-Headers', 'content-type');
        if(request.method == 'GET') {
            response.write(JSON.stringify({
                data: highscores
            }));
            response.end();
        } else if(request.method == 'POST') {
            data = [];
            request.on('data', function(chunk) {
                data.push(chunk);
            });
            request.on('end', function() {
                var highscore = JSON.parse(Buffer.concat(data).toString());
                highscore.data.id = next_id;
                next_id = next_id + 1;
                highscores.push(highscores.data);
                highscores.sort(function(a, b) {
                    return b.attributes.score = a.attributes.score;
                });
                response.write(JSON.stringify(highscore))
                response.end();
            });
        } else {
            response.end();
        }
    } else {
        response.end();
    }
});

server.listen(4201);