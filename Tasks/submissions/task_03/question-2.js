import Component from '@ember/component';

export default Component.extend({
    didInsertElement: function () {
        var stage = new createjs.Stage(this.$('#thestage')[0]);
        var board = new createjs.Shape();
        var graphics = board.graphics;

        graphics.beginFill('#ffffff');
        graphics.drawRect(100,100,100,100);
        stage.addChild(board);

        var tick = new createjs.Shape();
        graphics = tick.graphics;

        graphics.beginStroke('#00ff00');
        graphics.setStrokeStyle(10);
        graphics.moveTo(125, 150);
        graphics.lineTo(150,175);
        graphics.lineTo(175,125);

        stage.addChild(tick);


        stage.update();

    }
});
