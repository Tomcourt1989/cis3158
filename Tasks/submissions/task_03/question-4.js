import Component from '@ember/component';

export default Component.extend({
    didInsertElement: function () {
        var stage = new createjs.Stage(this.$('#thestage')[0]);
        var circle = new createjs.Shape();
        var graphics = circle.graphics;

        //White background required
        graphics.beginStroke('#000000');
        graphics.setStrokeStyle(10);
        graphics.drawCircle(100,100,30);
        graphics.moveTo(100,125);
        graphics.lineTo(100,195);

    
        stage.addChild(circle);

        var block = new createjs.Shape();
        var graphics = block.graphics;

        graphics.beginFill('#ffffff');
        graphics.setStrokeStyle(10);
        graphics.drawRect(50,75,45,60);

        stage.addChild(block);

        var rect = new createjs.Shape();
        var graphics = rect.graphics;

        graphics.beginStroke('#ffffff');
        graphics.setStrokeStyle(10);
        graphics.moveTo(100,165);
        graphics.lineTo(100,185);

        stage.addChild(rect);

        stage.update();

    }
});
