import Component from '@ember/component';

export default Component.extend({
    didInsertElement: function () {
        var stage = new createjs.Stage(this.$('#thestage')[0]);
        var box1 = new createjs.Shape();
        var graphics = box1.graphics;

        graphics.beginStroke('#ffff00');
        graphics.setStrokeStyle(10);
        graphics.drawRect(10,10,300,200);

        stage.addChild(box1);

        var box2 = new createjs.Shape();
        var graphics = box2.graphics;

        graphics.beginStroke('#ffff00');
        graphics.setStrokeStyle(10);
        graphics.drawRect(30,30,260,160);

        stage.addChild(box2);

        var box3 = new createjs.Shape();
        var graphics = box3.graphics;

        graphics.beginStroke('#ffff00');
        graphics.setStrokeStyle(10);
        graphics.drawRect(50,50,220,120);

        stage.addChild(box3);

        box.visible = false;

        stage.update();

    }
});
