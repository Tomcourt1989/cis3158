import Component from '@ember/component';

export default Component.extend({
    didInsertElement: function() {
        var stage = new createjs.Stage(this.$('#thestage')[0]);
        var board = new createjs.Shape();
        var graphics = board.graphics;

        graphics.beginFill('#ADD8E6');
        graphics.drawRect(0,0,500,500);

        stage.addChild(board);

        var boatbody = new createjs.Shape();
        var graphics = boatbody.graphics;

        graphics.beginFill('#a9a9a9');
        graphics.drawRect(150,100,200,150);

        stage.addChild(boatbody);

        var boat = new createjs.Shape();
        var graphics = boat.graphics;

        graphics.beginFill('#a9a9a9');
        graphics.drawRect(75,150,275,100);
        graphics.beginStroke('#a9a9a9');
        graphics.setStrokeStyle(10);
        graphics.moveTo(350,155);
        graphics.lineTo(450,155);
        graphics.lineTo(350,240);
        graphics.closePath();

        stage.addChild(boat);

        var window = new createjs.Shape();
        var graphics = window.graphics;

        graphics.beginFill('#ffffff');
        graphics.beginStroke('#000000');
        graphics.setStrokeStyle(10);
        graphics.drawCircle(325, 125, 15);
        graphics.beginStroke('#000000');
        graphics.setStrokeStyle(10);
        graphics.drawCircle(275, 125, 15);
        graphics.beginStroke('#000000');
        graphics.setStrokeStyle(10);
        graphics.drawCircle(225, 125, 15);

        stage.addChild(window);

        var water = new createjs.Shape()
        var graphics = water.graphics;

        graphics.beginFill('#0E86D4');
        graphics.drawRect(0,225,500,500);
        graphics.beginStroke('#0E86D4');
        graphics.setStrokeStyle(10);
        graphics.moveTo(0, 225);
        graphics.bezierCurveTo(25, 225, 50, 150, 150, 225);
        graphics.bezierCurveTo(150, 225, 225, 150, 300, 225);
        graphics.bezierCurveTo(350, 225, 400, 150, 500, 225);


        stage.addChild(water);

        stage.update();
    }
});
