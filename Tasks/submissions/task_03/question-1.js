import Component from '@ember/component';

export default Component.extend({
    didInsertElement: function() {
        var stage = new createjs.Stage(this.$('#thestage')[0]);
        var board = new createjs.Shape();
        var graphics = board.graphics;

        graphics.beginFill('#ffffff');
        graphics.drawRect(0,0,200,300);
        stage.addChild(board);

        var box = new createjs.Shape();
        graphics = box.graphics;

        graphics.beginFill('#ffff00');
        graphics.beginStroke('#ff0000');
        graphics.drawRect(100,75,25,50);
        stage.addChild(box);

        stage.update();
    }
});
