import Component from '@ember/component';

export default Component.extend({
    hello: true,

    actions: {
        switch: function() {
            if(this.hello == true) {
                this.set('hello', false);
            } else {
                this.set('hello', true);
            }
        }
    }
});
