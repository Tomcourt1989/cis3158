import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        'create': function(code, title, summary, academic_year, number_of_students) {
            var controller = this;
            var module = controller.store.createRecord('module', {
                code: code,
                title: title,
                summary: summary,
                academic_year: academic_year,
                number_of_students: number_of_students
            });
            module.save().then(function() {
                controller.store.unloadAll();
                controller.transitionToRoute('modules');
            });
        }
    }
});
