import DS from 'ember-data';

export default DS.Model.extend({
    code: DS.attr('string'),
    title: DS.attr('string'),
    summary: DS.attr('string'),
    academic_year: DS.attr('number'),
    number_of_students: DS.attr('number')
});
